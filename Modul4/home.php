<?php
session_start();
?>
<!doctype html>
<html lang="en">

<head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


    <title>EAD Store</title>
</head>

<body>
    <?php
    if(isset($_SESSION["login"])){
        echo '<nav class="navbar navbar-light bg-light">
        <a class="navbar-brand" href="home.php">
            <img src="ead.png" height="30px" class="d-inline-block align-top" alt="">
        </a>
        <div style="float: right;">
            <table>
                <tr>
                <td><a href="cart.php"><img src="cart.png" alt="Cart" width="20px"></a></td>
                <td>
                <div class="bs-example" style="padding: 5px;">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">';
                echo $_SESSION["userLogin"];
                echo '</a>
                <div class="dropdown-menu dropdown-menu-right">
                <a href="profile.php" class="dropdown-item">Profile</a>
                <div class="dropdown-divider"></div>
                <a href="logout.php" class="dropdown-item">Logout</a>
                </div>
                </div>
                </td>
                </tr>
            </table>


        </div>
    </nav>';
    }else{
        echo '<nav class="navbar navbar-light bg-light">
        <a class="navbar-brand" href="home.php">
            <img src="ead.png" height="30px" class="d-inline-block align-top" alt="">
        </a>
        <div style="float: right;">
            <a href="" data-toggle="modal" data-target="#loginButton">Login</a>
            <label> </label>
            <a href="config.php" data-toggle="modal" data-target="#regisButton">Register</a>
        </div>
    </nav>';
    }
    ?>
    <!-- Belum Login -->




    <!-- Login -->
    <div class="modal fade" id="loginButton" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                <h4 class="modal-title">Login</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <form action="config.php" method="POST">
                    <div class="modal-body">
                    <label>Email Address</label>
                    <div class="input-group flex-nowrap">
                    <input type="email" name="email" class="form-control" placeholder="Email" aria-label="Email"
                            aria-describedby="addon-wrapping">
                    </div><br>
                    <label>Password</label>
                    <div class="input-group flex-nowrap">
                    <input type="password" name="password" class="form-control" placeholder="Password"
                        aria-label="Password" aria-describedby="addon-wrapping">
                    </div>
                    </div>
                    <div class=" modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <input type="submit" name="login" class="btn btn-primary" value="Login">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Regis -->
    <div class="modal fade" id="regisButton" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Register</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <form action="config.php" method="POST">
                    <div class="modal-body">
                        <label>Email Address</label>
                        <div class="input-group flex-nowrap">
                        <input type="email" name="rEmail" class="form-control" placeholder="Email"
                                aria-label="Email" aria-describedby="addon-wrapping">
                        </div><br>
                        <label>Username</label>
                        <div class="input-group flex-nowrap">
                        <input type="text" name="rUsername" class="form-control" placeholder="Username"
                                aria-label="Username" aria-describedby="addon-wrapping">
                        </div><br>
                        <label>Password</label>
                        <div class="input-group flex-nowrap">
                        <input type="password" name="rPass" class="form-control" placeholder="Password"
                                aria-label="Password" aria-describedby="addon-wrapping">
                        </div><br>
                        <label>Confirm Password</label>
                        <div class="input-group flex-nowrap">
                         <input type="password" name="cPass" class="form-control" placeholder="Confirm Password"
                                aria-label="Confirm Password" aria-describedby="addon-wrapping">
                        </div>
                    </div>
                    <div class=" modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <input type="submit" name="register" class="btn btn-primary" value="Register">
                    </div>
                </form>
            </div>
        </div>
    </div>



    <form action="config.php" method="GET">
        <div style="width: 60%;margin:auto;">
            <div class="card" style='background-image:url("photo/gradient2.png");background-size:cover;color: white;'>
                <div style="margin-left:10px;">
                <br>
                <h1>Hello Coders</h1>
                <p>Welcome to our store, please take a look for the products you might buy.</p>
                <br>
                </div>
            </div>
            <div class="card-group">
                <div class="card">
                <img src="photo/web.png" class="card-img-top" alt="web logo">
                <div class="card-body">
                <h5 class="card-title">Learning Basic Web Programming Rp.210.000,-</h5>
                    <p class="card-text">Want to able to make a website? Learn basic components such as HTML,
                            CSS and JavaScript in this class curiculum.</p>
                </div>
                </div>
                <div class="card">
                    <img src="photo/java.png" class="card-img-top" alt="java logo">
                    <div class="card-body">
                    <h5 class="card-title">Starting Programming in Java Rp.150.000,-</h5>
                    <p class="card-text">Learn Java Language for you who want to learn the most popular
                            Object-Oriented Programming (OOP) concept for developing applications.</p>
                    </div>
                </div>
                <div class="card">
                    <img src="photo/python.png" class="card-img-top" alt="python logo">
                    <div class="card-body">
                    <h5 class="card-title">Starting Programming in Python Rp.200.000,-</h5>
                    <p class="card-text">Learn Python - Fundamental various current industry trends: Data
                            Science, Machine Learning, Infrastructure Management.</p>
                    </div>
                </div>
            </div>
            <div class="card-group">
                <div class="card">

                    <div class="card-body">
                    <center><input type="submit" name="buy1" class="btn btn-primary btn-block" value="BUY"></center>
                    </div>
                </div>
                <div class="card">

                    <div class="card-body">
                        <center><input type="submit" name="buy2" class="btn btn-primary btn-block" value="BUY"></center>
                    </div>
                </div>
                <div class="card">

                <div class="card-body">
                <center><input type="submit" name="buy3" class="btn btn-primary btn-block" value="BUY"></center>
                </div>
                </div>
            </div>
        </div>
    <center><h6>©EAD STORE</h6></center>

    </form>



    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
</body>

</html>