<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon; //generate time

class PostsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1; $i <= 2; $i++) {
          DB::table('posts')->insert([
            'user_id' => $i,
            'caption' => 'Coba lihat foto saya',
            'image' => 'img/'.$i.'.jpg',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
          ]);
        }

        DB::table('posts')->insert([
          'user_id' => 2,
          'caption' => 'Saya ganteng',
          'image' => 'img/yoga2.jpg',
          'created_at' => Carbon::now(),
          'updated_at' => Carbon::now(),
        ]);
    }
}
