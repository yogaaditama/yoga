<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon; //generate time and date

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
          'name' => 'Yoga Aditama',
          'email' => 'yoga13@gmail.com',
          'password' => bcrypt('123'),
          'title' => 'ini title',
          'description' => 'lorem ipsum sir amet dolor.',
          'url' => 'instagram.com/',
          'avatar' => 'avatars/erza.jpg',
          'created_at' => Carbon::now(),
          'updated_at' => Carbon::now(),
        ]);

        // DB::table('users')->insert([
        //   'name' => 'Putra Ganteng',
        //   'email' => 'putra@gmail.com',
        //   'password' => bcrypt('putra'),
        //   'avatar' => 'avatars/putra.jpg',
        //   'title' => 'ini title 2',
        //   'description' => 'Saya ganteng dan menawan',
        //   'url' => 'facebook.com/zasiem',
        //   'created_at' => Carbon::now(),
        //   'updated_at' => Carbon::now(),
        // ]);
    }
}
