<?php

use Illuminate\Database\Seeder;

class KomentarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      for ($i=0; $i < 4; $i++) {
        DB::table('komentar_posts')->insert([
          'user_id' => 2,
          'post_id' => 1,
          'comment' => 'testing comment',
        ]);
      }

      for ($i=0; $i < 4; $i++) {
        DB::table('komentar_posts')->insert([
          'user_id' => 1,
          'post_id' => 2,
          'comment' => 'testing comment 2',
        ]);
      }

    }
}
