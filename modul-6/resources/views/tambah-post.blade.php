@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center mx-5">
    <div class="col-12">
      <h1>Tambah Post</h1>
    </div>
    <div class="col-12">
      <form method="post" action="/tambah-post/proses" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
          <label>Post Caption</label>
          <input type="text" class="form-control" name="caption">
        </div>
        <div class="form-group">
          <label>Post Image</label>
          <input type="file" name="image" class="form-control-file">
        </div>
        <button type="submit" class="btn btn-primary">Add New Post</button>
      </form>
    </div>
  </div>
</div>
@endsection
