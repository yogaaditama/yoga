@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center my-5">
    <div class="col-3 text-center">
      <img class="rounded-circle" src="{{ url('storage/'.$data->avatar) }}" alt="" height="150px" width="150px">
    </div>
    <div class="col-7">
      <h2>{{ $data->name }}</h2>
      <br>
      <a href="/edit-profile">Edit Profile</a>
      <p><b>{{ $data->posts->count() }}</b> Post</p>
      <div class="">
        <b>{{ $data->title }}</b><br>
        {{ $data->description }}<br>
        <a href="https://{{ $data->url }}">{{ $data->url }}</a><br>
      </div>
    </div>
    <div class="col-2">
      <a href="/tambah-post">Add New Post</a>
    </div>
  </div>
  <div class="row justify-content-left mb-5">
    @foreach($data->posts as $post)
    <div class="col-md-4">
      <a href="/post/{{ $post->id }}">
        <img src="{{ url('storage/'.$post->image) }}" alt="foto" height="300px" width="100%">
      </a>
    </div>
    @endforeach
  </div>
</div>
</div>
@endsection
