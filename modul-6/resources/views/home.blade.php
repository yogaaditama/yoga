@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    @foreach($posts as $post)
    <div class="col-md-8">
      <div class="card my-3">
        <div class="card-header">
          <img class="rounded-circle" src="{{ url('/storage/'.$post->users->avatar) }}" alt="foto" height="50px" width="50px">
          {{ $post->users->name }}
        </div>
        <div class="card-body">
          <a href="{{ url('/post/'.$post->id) }}">
          <img src="{{ url('/storage/'.$post->image) }}" alt="foto-konten" width="100%">
          </a>
        </div>
        <div class="card-footer">
          <div class="emoticon">
            <form action="/likes" method="post" style="display:inline">
              @csrf
              <button type="submit" name="button_likes" class="btn" value="{{ $post->id }}"><i class="fa fa-heart-o"></i></button>
            </form>
            <button type="button" name="button" class="btn"><i class="fa fa-comment"></i></button>
          </div>
          <div class="detail">
            <p>{{ $post->likes }} Likes</p>
            <p>
              <b>{{ $post->users->email }}</b> {{ $post->caption }}
            </p>
            <hr>
              @foreach($post->komentar_post as $komentar)
                <p>
                  <b>{{ $komentar->users->email }}</b> {{ $komentar->comment }}
                </p>
              @endforeach
          </div>
          <form action="/tambah_komen" method="post">
            @csrf
            <div class="input-group mb-3">
              <input type="text" class="form-control" placeholder="Recipient's username" name="komentar">
              <div class="input-group-append">
                <button class="btn btn-outline-secondary" type="submit" name="button_komen" value="{{ $post->id }}">Post</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
    @endforeach
  </div>
</div>
@endsection
