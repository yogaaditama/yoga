<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\PostsModel as Posts;
use App\Komentar_posts as Komentar_posts;
use Carbon\Carbon;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

      $data = Posts::with('users')
      ->with('komentar_post')
      ->orderBy('id', 'DESC')
      ->get();

      return view('home', ['posts' => $data] );
    }


}
