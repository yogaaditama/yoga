<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Komentar_posts extends Model
{
    protected $table = "komentar_posts";

    public function users(){
      return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function posts(){
      return $this->belongsTo('App\PostsModel', 'post_id', 'id');
    }
}
