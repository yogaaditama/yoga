<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/profile', 'ProfileController@index');

Route::get('/edit-profile', 'ProfileController@edit_index');

Route::post('/edit-profile/proses', 'ProfileController@edit_proses');

Route::post('/likes', 'PostController@likes');

Route::post('/tambah_komen', 'PostController@komentar');

Route::get('/post/{id}', 'PostController@detail');

Route::get('/tambah-post', 'PostController@index');

Route::post('/tambah-post/proses', 'PostController@tambah');
