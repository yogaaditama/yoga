<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'yoga Raevan',
            'email' => 'yoga'.'@gmail.com',
            'password' => bcrypt('password'),
            'avatar' => 'img/2.jpg',
            ]);

     
    }
}
