<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     DB::table('post')->insert([
            'user_id' => '1',
            'caption' => 'Hello, This is my first post!',
            'image' => 'img/1.jpg',
            
        ]);
        //
    //     DB::table('post')->insert([
    //         'user_id' => '1',
    //         'caption' => 'Hello, This is my second post!',
    //         'image' => 'img/foto2.jpg',
    //     ]);

    //     DB::table('post')->insert([
    //         'user_id' => '1',
    //         'caption' => 'Hello, This is my third post!',
    //         'image' => 'img/foto3.jpg',
    //     ]);
    }
}